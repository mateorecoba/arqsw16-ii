package com.enviosya.util.enums;

/**
 *
 * @author mateo
 */
public enum EntityStatusEnum {

    OK("Transaccion realizada correctamente."),
    NO_EXISTE("Entidad no encontrada."),
    ERROR_BD("Error al comunicarse con la base de datos."),
    ERROR_GENERICO("Error interno al servidor.");
    
    private String message;

    EntityStatusEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
}
