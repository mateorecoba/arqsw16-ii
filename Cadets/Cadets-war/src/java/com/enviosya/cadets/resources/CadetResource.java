package com.enviosya.cadets.resources;

import com.enviosya.cadets.beans.CadetBean;
import com.enviosya.cadets.beans.CadetLocationBean;
import com.enviosya.cadets.dtos.CadetClosestShipmentDto;
import com.google.gson.Gson;
import com.enviosya.cadets.entities.Cadet;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.enviosya.util.enums.EntityStatusEnum;

/**
 * REST Web Service
 *
 * @author Diego
 */
@Path("cadet")
@RequestScoped
public class CadetResource {

    @Context
    private UriInfo context;

    private Response response;

    @EJB
    private CadetBean cadetBean;
    
    @EJB
    private CadetLocationBean cadetLocationBean;

    public CadetResource() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCadet(String params) {
        Gson gson = new Gson();
        Cadet cadet = gson.fromJson(params, Cadet.class);
        cadet = cadetBean.add(cadet);
        if (cadet != null) {
            response = Response.ok(gson.toJson(cadet)).build();
        } else {
            response = Response.serverError().build();
        }
        return response;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyCadet(String params) {
        Gson gson = new Gson();
        Cadet cadet = gson.fromJson(params, Cadet.class);
        EntityStatusEnum message = cadetBean.modify(cadet);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCadet(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        EntityStatusEnum message = cadetBean.delete(id);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    @GET
    @Path("findbyid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findCadetById(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        Cadet cadet = cadetBean.findById(id);
        if (cadet != null) {
            response = Response.ok(gson.toJson(cadet)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }

    @GET
    @Path("findbyci")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findCadetByCi(@QueryParam(value = "ci") Integer ci) {
        Gson gson = new Gson();
        Cadet cadet = cadetBean.findByCi(ci);
        if (cadet != null) {
            response = Response.ok(gson.toJson(cadet)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }

    @GET
    @Path("findbyname")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findCadetByName(@QueryParam(value = "firstname") String firstName,
            @QueryParam(value = "surename") String sureName) {
        Gson gson = new Gson();
        ArrayList<Cadet> cadetsList = cadetBean.findByName(firstName, sureName);
        if (!cadetsList.isEmpty()) {
            response = Response.ok(gson.toJson(cadetsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }

    @GET
    @Path("getclosestcadets")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getClosestCadets(@QueryParam(value = "address") String address) {
        Gson gson = new Gson();
        ArrayList<CadetClosestShipmentDto> cadetsList = cadetLocationBean.getClosestCadets(address);
        if (!cadetsList.isEmpty()) {
            response = Response.ok(gson.toJson(cadetsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }
    
}
