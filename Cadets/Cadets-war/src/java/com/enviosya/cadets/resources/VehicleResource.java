package com.enviosya.cadets.resources;

import com.enviosya.cadets.beans.VehicleBean;
import com.google.gson.Gson;
import com.enviosya.cadets.entities.Vehicle;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.enviosya.util.enums.EntityStatusEnum;

/**
 * REST Web Service
 *
 * @author Diego
 */
@Path("vehicle")
@RequestScoped
public class VehicleResource {

    @Context
    private UriInfo context;

    private Response response;
    
    @EJB
    private VehicleBean vehicleBean;
    
    public VehicleResource() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addVehicle(String params) {
        Gson gson = new Gson();
        Vehicle vehicle = gson.fromJson(params, Vehicle.class);
        vehicle = vehicleBean.add(vehicle);
        if (vehicle != null) {
            response = Response.ok(gson.toJson(vehicle)).build();
        } else {
            response = Response.serverError().build();
        }
        return response;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyVehicle(String params) {
        Gson gson = new Gson();
        Vehicle vehicle = gson.fromJson(params, Vehicle.class);
        EntityStatusEnum message = vehicleBean.modify(vehicle);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @PUT
    @Path("modifyattributes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyVehicleAttributes(@FormParam(value = "id") Long id, 
            @FormParam(value = "licenseplate") String licensePlate, 
            @FormParam(value = "description") String description) {
        Gson gson = new Gson();
        EntityStatusEnum message = vehicleBean.modifyAttributes(id, licensePlate, description);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @PUT
    @Path("associateCadet")
    @Produces(MediaType.APPLICATION_JSON)
    public Response associateVehicleCadet(@FormParam(value = "vehicleid") Long vehicleId, 
            @FormParam(value = "cadetid") Long cadetId) {
        Gson gson = new Gson();
        EntityStatusEnum message = vehicleBean.associateCadet(vehicleId, cadetId);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteVehicle(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        EntityStatusEnum message = vehicleBean.delete(id);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @GET
    @Path("findbyid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findVehicleById(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        Vehicle vehicle = vehicleBean.findById(id);
        if (vehicle != null) {
            response = Response.ok(gson.toJson(vehicle)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }
    
    @GET
    @Path("findbylicenseplate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findVehicleByLicensePlate(@QueryParam(value = "licenseplate") String licensePlate) {
        Gson gson = new Gson();
        Vehicle vehicle = vehicleBean.findByLicensePlate(licensePlate);
        if (vehicle != null) {
            response = Response.ok(gson.toJson(vehicle)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }      
        return response;
    }
}