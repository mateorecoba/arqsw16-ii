package com.enviosya.cadets.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author mateo
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "findVehicleByLicensePlate", 
                    query = "SELECT v FROM Vehicle v WHERE v.licensePlate LIKE :licenseplate")
    })
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(unique = true, length = 10)
    private String licensePlate;
    
    @Column(length = 255)
    private String description;

    @ManyToOne
    private Cadet cadet;
        
    public Vehicle() {
    }
    
    public Vehicle(String licensePlate, String description, Cadet cadet) {
        this.licensePlate = licensePlate;
        this.description = description;
        this.cadet = cadet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Cadet getCadet() {
        return cadet;
    }

    public void setCadet(Cadet cadet) {
        this.cadet = cadet;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "id=" + id + ", matricula=" + licensePlate 
                + ", descripcion=" + description + ", cadete=" + cadet + '}';
    }
    
}
