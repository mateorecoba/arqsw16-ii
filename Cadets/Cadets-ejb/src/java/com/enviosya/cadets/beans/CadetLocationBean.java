package com.enviosya.cadets.beans;

import com.enviosya.cadets.dtos.CadetClosestShipmentDto;
import com.enviosya.cadets.entities.Cadet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author Diego
 */
@Stateless
@LocalBean
public class CadetLocationBean implements CadetLocationBeanLocal {

    @EJB
    private CadetBean cadetBean;
    
    private HashMap<Long, String> cadetLocation;
    
    public CadetLocationBean() {
        cadetLocation = new HashMap<>();
        cadetLocation.put(1L, "Dir 1");
        cadetLocation.put(2L, "Dir 2");
        cadetLocation.put(3L, "Dir 3");
        cadetLocation.put(4L, "Dir 4");
    }
    
    @Override
    public ArrayList<CadetClosestShipmentDto> getClosestCadets(String clientAddress) {
        Double latClient = getLat(clientAddress);
        Double lngClient = getLng(clientAddress);
        ArrayList<CadetClosestShipmentDto> ret = new ArrayList<>();
        ArrayList<Long> idList = new ArrayList<>();
        ArrayList<Double> distList = new ArrayList<>();
        Double maxDist = 0.0;
        Iterator<Long> cadetsId = cadetLocation.keySet().iterator();
        while (cadetsId.hasNext()) {
            Long cadetId = cadetsId.next();
            String cadetAddress = cadetLocation.get(cadetId);
            Double latCadet = getLat(cadetAddress);
            Double lngCadet = getLng(cadetAddress);
            Double dist = distance(latClient, lngClient, latCadet, lngCadet);
            if (idList.size() < 4) {
                if (dist > maxDist) {
                    maxDist = dist;
                }
                idList.add(cadetId);
                distList.add(dist);
            } else {
                if (dist < maxDist) {
                    idList.remove(distList.indexOf(maxDist));
                    distList.remove(maxDist);
                    maxDist = dist;
                    idList.add(cadetId);
                    distList.add(dist);
                }
            }
        }
        for (int i = 0; i < idList.size(); i++) {
            Cadet cadet = cadetBean.findById(idList.get(i));
            ret.add(new CadetClosestShipmentDto(cadet));
        }                
        return ret;         
    }
    
    private double getLat(String address) {
        return 0;
    }
    
    private double getLng(String address) {
        return 0;
    }
    
    private double distance(double lat1, double lng1, double lat2, double lng2) {  
        double rdEarth = 6371; 
        double distanceLat = Math.toRadians(lat2 - lat1);  
        double distanceLng = Math.toRadians(lng2 - lng1);  
        double sindLat = Math.sin(distanceLat / 2);  
        double sindLng = Math.sin(distanceLng / 2);  
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)  
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));  
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));  
        double distance = rdEarth * va2;  
        return distance;  
    }  

}