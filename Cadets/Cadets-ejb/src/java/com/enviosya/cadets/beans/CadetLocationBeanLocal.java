package com.enviosya.cadets.beans;

import com.enviosya.cadets.dtos.CadetClosestShipmentDto;
import java.util.ArrayList;
import javax.ejb.Local;

/**
 *
 * @author Diego
 */
@Local
public interface CadetLocationBeanLocal {
 
    ArrayList<CadetClosestShipmentDto> getClosestCadets(String clientAddress);
    
}
