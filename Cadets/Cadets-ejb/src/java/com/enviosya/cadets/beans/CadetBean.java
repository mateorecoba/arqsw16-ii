package com.enviosya.cadets.beans;

import com.enviosya.cadets.entities.Cadet;
import com.enviosya.cadets.persistence.EntitiesManagerBean;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import com.enviosya.util.enums.EntityStatusEnum;
import com.enviosya.log.logger.CustomLogger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;

/**
 *
 * @author Diego
 */
@Stateless
@LocalBean
public class CadetBean implements CadetBeanLocal {

    @EJB
    private EntitiesManagerBean cadetEntityManagerBean; 

    private CustomLogger log = new CustomLogger(CadetBean.class);
    
    @Override
    public Cadet add(Cadet cadet) {
        log.debug("Agregando cadete " + cadet.toString());
        try {
            cadetEntityManagerBean.persistCadet(cadet);
            log.debug("Cadete " + cadet.toString() + " creado correctamente");
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al agregar Cadete", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al agregar Cadete", e);
            return null;
        }
        return cadet;
    }

    @Override
    public EntityStatusEnum modify(Cadet cadet) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Cadet aux = (Cadet) cadetEntityManagerBean.find(Cadet.class, cadet.getId());
            if (aux != null) {
                aux.setCi(cadet.getCi());
                aux.setFirstName(cadet.getFirstName());
                aux.setSureName(cadet.getSureName());
                aux.setEmail(cadet.getEmail());
                cadetEntityManagerBean.mergeCadet(aux);
                log.debug("Cadete " + aux.toString() + " modificado correctamente");
            } else {
                log.debug("Cadete " + cadet.getId() + " no existe");
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al modificar Cadete", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al modificar Cadete", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        
        return ret;
    }

    @Override
    public EntityStatusEnum delete(Long id) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Cadet cadet = (Cadet) cadetEntityManagerBean.find(Cadet.class, id);
            if (cadet != null) {
                cadetEntityManagerBean.removeCadet(cadet);
                log.info("Cadete " + cadet.getId() + " eliminado correctamente");
            } else {
                log.debug("Cadete " + cadet.toString() + " no existe");
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al eliminar Cadete", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al eliminar Cadete", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public Cadet findById(Long id) {
        log.debug("Buscando Cadete por id " + id );
        try {
            //Cadet cadet = (Cadet) cadetEntitiManagerBean.find(Cadet.class, id);
            Cadet cadet = new Cadet(1L, 1, "a", "a", "a");       
            return cadet;
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Cadete por id", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Cadete por id", e);
            return null;
        }
    }

    @Override
    public Cadet findByCi(Integer ci) {
        try {
            Cadet cadet = cadetEntityManagerBean.findCadetByCi(ci);
            return cadet;      
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Cadete por CI", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Cadete por CI", e);
            return null;
        }
    }

    @Override
    public ArrayList<Cadet> findByName(String firstName, String sureName) {
        ArrayList<Cadet> ret;
        try {
            List<Cadet> lista = cadetEntityManagerBean.findCadetByName(firstName, sureName);
            ret = new ArrayList<>(lista);         
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Cadete por nombre", e);
            ret = new ArrayList<>();
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Cadete por nombre", e);
            ret = new ArrayList<>();
        }       
        return ret;
    }
    
}