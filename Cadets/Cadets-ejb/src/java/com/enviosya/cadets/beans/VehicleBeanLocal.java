package com.enviosya.cadets.beans;

import com.enviosya.cadets.entities.Vehicle;
import javax.ejb.Local;
import com.enviosya.util.enums.EntityStatusEnum;

/**
 *
 * @author Diego
 */
@Local
public interface VehicleBeanLocal {
    
    Vehicle add(Vehicle vehicle);

    EntityStatusEnum modify(Vehicle vehicle);
    
    EntityStatusEnum modifyAttributes(Long id, String licensePlate, String description);

    EntityStatusEnum associateCadet(Long vehicleId, Long cadetId);
    
    EntityStatusEnum delete(Long id);

    Vehicle findById(Long id);

    Vehicle findByLicensePlate(String licensePlate);
    
}