package com.enviosya.cadets.beans;

import com.enviosya.cadets.entities.Cadet;
import java.util.ArrayList;
import javax.ejb.Local;
import com.enviosya.util.enums.EntityStatusEnum;

/**
 *
 * @author Diego
 */
@Local
public interface CadetBeanLocal {
    
    Cadet add(Cadet cadet);

    EntityStatusEnum modify(Cadet cadet);

    EntityStatusEnum delete(Long id);

    Cadet findById(Long id);

    Cadet findByCi(Integer ci);

    ArrayList<Cadet> findByName(String firstName, String sureName);
    
}