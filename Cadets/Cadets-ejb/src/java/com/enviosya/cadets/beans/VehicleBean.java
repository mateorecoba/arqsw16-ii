package com.enviosya.cadets.beans;

import com.enviosya.cadets.entities.Cadet;
import com.enviosya.cadets.entities.Vehicle;
import com.enviosya.cadets.persistence.EntitiesManagerBean;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import com.enviosya.log.logger.CustomLogger;
import com.enviosya.util.enums.EntityStatusEnum;
import javax.ejb.EJB;

/**
 *
 * @author Diego
 */
@Stateless
@LocalBean
public class VehicleBean implements VehicleBeanLocal {

    @EJB
    private EntitiesManagerBean entitiesManagerBean;
    
    private CustomLogger log = new CustomLogger(VehicleBean.class);

    @Override
    public Vehicle add(Vehicle vehicle) {
        try {
            entitiesManagerBean.persistVehicle(vehicle);
            log.info("Vehiculo " + vehicle.getId() + " creado correctamente");
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al agregar Vehiculo", e);
        } catch (Exception e) {
            log.error("Excepcion generica al agregar Vehiculo", e);
        }
        return vehicle;
    }

    @Override
    public EntityStatusEnum modify(Vehicle vehicle) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Vehicle aux = (Vehicle) entitiesManagerBean.find(Vehicle.class, vehicle.getId());
            if (aux != null) {
                aux.setLicensePlate(vehicle.getLicensePlate());
                aux.setDescription(vehicle.getDescription());
                aux.setCadet(vehicle.getCadet());
                entitiesManagerBean.mergeVehicle(aux);
                log.info("Vehiculo " + aux.getId() + " modificado correctamente");
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al modificar Vehiculo", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al modificar Vehiculo", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public EntityStatusEnum modifyAttributes(Long id, String licensePlate, String description) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Vehicle vehicle = (Vehicle) entitiesManagerBean.find(Vehicle.class, id);
            if (vehicle != null) {
                vehicle.setLicensePlate(licensePlate);
                vehicle.setDescription(description);
                entitiesManagerBean.mergeVehicle(vehicle);
                log.info("Vehiculo " + vehicle.getId() + " modificado correctamente");
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al modificar atributos de Vehiculo", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al modificar atributos de Vehiculo", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public EntityStatusEnum associateCadet(Long vehicleId, Long cadetId) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Vehicle vehicle = (Vehicle) entitiesManagerBean.find(Vehicle.class, vehicleId);
            Cadet cadet = (Cadet) entitiesManagerBean.find(Cadet.class, cadetId);
            if (vehicle != null) {
                vehicle.setCadet(cadet);
                entitiesManagerBean.mergeVehicle(vehicle);
                log.info("Vehiculo " + vehicle.getId() + " asociado correctamente al cadete " + cadet.getId());
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al asociar Cadete con Vehiculo", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al asociar Cadete con Vehiculo", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public EntityStatusEnum delete(Long id) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Vehicle vehicle = (Vehicle) entitiesManagerBean.find(Vehicle.class, id);
            if (vehicle != null) {
                entitiesManagerBean.removeVehicle(vehicle);
                log.info("Vehiculo " + vehicle.getId() + " eliminado correctamente");
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al eliminar Vehiculo", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al eliiminar Vehiculo", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public Vehicle findById(Long id) {
        log.debug("Buscando Vehiculo por id " + id);
        try {
            Vehicle vehicle = (Vehicle) entitiesManagerBean.find(Vehicle.class, id);
            log.debug("Vehiculo encontrado " + vehicle.toString());
            return vehicle;
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Vehiculo por id", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Vehiculo por id", e);
            return null;
        }
    }

    @Override
    public Vehicle findByLicensePlate(String licensePlate) {
        try {
            Vehicle vehicle = entitiesManagerBean.findVehicleByLicensePlate(licensePlate);
            return vehicle;
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Vehiculo por Matricula", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Vehiculo por Matricula", e);
            return null;
        }
    }
    
}
