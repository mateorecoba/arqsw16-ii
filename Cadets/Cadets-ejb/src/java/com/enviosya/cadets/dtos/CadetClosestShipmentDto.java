package com.enviosya.cadets.dtos;

import com.enviosya.cadets.entities.Cadet;

/**
 *
 * @author Diego
 */
public class CadetClosestShipmentDto {
    
    private Long id;
    private String firstName;
    private String sureName;
    private String email;

    public CadetClosestShipmentDto(Cadet cadet) {
        this.id = cadet.getId();
        this.firstName = cadet.getFirstName();
        this.sureName = cadet.getSureName();
        this.email = cadet.getEmail();
    }

    public CadetClosestShipmentDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
        
}