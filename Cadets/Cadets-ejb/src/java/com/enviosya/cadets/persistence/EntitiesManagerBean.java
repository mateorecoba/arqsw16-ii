package com.enviosya.cadets.persistence;

import com.enviosya.cadets.entities.Cadet;
import com.enviosya.cadets.entities.Vehicle;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mateo
 */
@Singleton
@LocalBean
public class EntitiesManagerBean {

    @PersistenceContext
    private EntityManager entityManager;

    public void persistCadet(Cadet cadet) {
        entityManager.persist(cadet);
    }

    public void mergeCadet(Cadet aux) {
        entityManager.merge(aux);
    }

    public void removeCadet(Cadet cadet) {
        entityManager.remove(cadet);
    }

    public Cadet findCadetByCi(Integer ci) {
        return entityManager.createNamedQuery("findCadetByCi", Cadet.class)
                .setParameter("ci", ci)
                .getSingleResult();
    }

    public List<Cadet> findCadetByName(String firstName, String sureName) {
        return entityManager.createNamedQuery("findCadetByName", Cadet.class)
                .setParameter("firstName", firstName)
                .setParameter("sureName", sureName)
                .getResultList();
    }

    public void persistVehicle(Vehicle vehicle) {
        entityManager.persist(vehicle);
    }

    public void mergeVehicle(Vehicle aux) {
        entityManager.merge(aux);
    }

    public void removeVehicle(Vehicle vehicle) {
        entityManager.remove(vehicle);
    }

    public Object find(Class clazz, Long id) {
        return entityManager.find(clazz, id);
    }

    public Vehicle findVehicleByLicensePlate(String licensePlate) {
        return entityManager.createNamedQuery("findVehicleByLicensePlate", Vehicle.class)
                .setParameter("licensePlate", licensePlate)
                .getSingleResult();
    }

}