package com.enviosya.shipments.utils;

/**
 *
 * @author mateo
 */
public enum ShipmentStatusEnum {
    OK("Transaccion realizada correctamente."),
    NO_EXISTE_VEHICULO("Vehiculo no encontrado."),
    NO_EXISTE_CLIENTE_REMITENTE("Cliente Remitente no encontrado."),
    NO_EXISTE_CLIENTE_RECEPTOR("Cliente Receptor no encontrado."),
    NO_EXISTE_CADETE("Cadete no encontrada."),    
    CADETE_VEHICULO_NO_ASOCIADOS("Cadete y Vehiculo no asociados."),
    VEHICULO_SIN_CADETE("El Vehiculo no tiene un cadete asociado"),
    ERROR_BD("Error al comunicarse con la base de datos."),
    ERROR_GENERICO("Error interno al servidor.");
    
    private String message;

    ShipmentStatusEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
