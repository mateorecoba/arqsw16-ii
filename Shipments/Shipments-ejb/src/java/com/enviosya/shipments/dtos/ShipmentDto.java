package com.enviosya.shipments.dtos;

import com.enviosya.shipments.utils.ShipmentStatusEnum;
import com.enviosya.shipments.entities.Shipment;
import java.util.ArrayList;

/**
 *
 * @author mateo
 */
public class ShipmentDto {

    private Shipment shipment;
    private ShipmentStatusEnum shipmentStatusEnum;
    private ArrayList<ShipmentClosestCadetDto> closestCadets;

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public ShipmentStatusEnum getShipmentStatusEnum() {
        return shipmentStatusEnum;
    }

    public void setShipmentStatusEnum(ShipmentStatusEnum entityManagerStatus) {
        this.shipmentStatusEnum = entityManagerStatus;
    }   

    public ArrayList<ShipmentClosestCadetDto> getClosestCadets() {
        return closestCadets;
    }

    public void setClosestCadets(ArrayList<ShipmentClosestCadetDto> closestCadets) {
        this.closestCadets = closestCadets;
    }  
    
}