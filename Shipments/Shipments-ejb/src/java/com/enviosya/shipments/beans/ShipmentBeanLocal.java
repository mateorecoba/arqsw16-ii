package com.enviosya.shipments.beans;

import com.enviosya.shipments.dtos.ShipmentDto;
import com.enviosya.shipments.entities.Shipment;
import com.enviosya.util.enums.EntityStatusEnum;
import java.util.ArrayList;
import javax.ejb.Local;

/**
 *
 * @author Diego
 */
@Local
public interface ShipmentBeanLocal {
    
    ShipmentDto add(Shipment shipment);
    
    ShipmentDto getClosestCadets(ShipmentDto shipmentDto);
    
    EntityStatusEnum setCadet(Long id, Long cadetId);
    
    EntityStatusEnum confirmReception(Long id);

    EntityStatusEnum modify(Shipment shipment);

    EntityStatusEnum delete(Long id);

    Shipment findById(Long id);
    
    ArrayList<Shipment> findBySender(Long clientId);
    
    ArrayList<Shipment> findByReceiver(Long clientId);
    
    ArrayList<Shipment> listCadetShipments(Long cadetId);   
    
}
