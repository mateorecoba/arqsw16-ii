package com.enviosya.shipments.beans;

import javax.ejb.EJB;
import com.enviosya.log.logger.CustomLogger;
import com.enviosya.shipments.dtos.ShipmentClosestCadetDto;
import com.enviosya.shipments.dtos.ShipmentDto;
import com.enviosya.shipments.entities.Shipment;
import com.enviosya.shipments.persistence.ShipmentEntityManagerBean;
import com.enviosya.shipments.utils.ShipmentStatusEnum;
import com.enviosya.util.enums.EntityStatusEnum;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;

/**
 *
 * @author Diego
 */
@Stateless
@LocalBean
public class ShipmentBean implements ShipmentBeanLocal {

    @EJB
    private ShipmentEntityManagerBean shipmentEntityManagerBean;

    private CustomLogger log = new CustomLogger(ShipmentBean.class);
    
    @Override
    public ShipmentDto add(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        try {
            shipmentDto = validateClients(shipment);
            if (shipmentDto.getShipmentStatusEnum() == ShipmentStatusEnum.OK) {
                try {
                    HttpResponse<String> response
                            = Unirest.get("http://localhost:8080/CostCalculation-war/costcalculation/getcost?id=1").asString();
                    Double shipmentCost = Double.parseDouble(response.getBody());
                    shipment.setShippingCost(shipmentCost);                   
                    shipmentDto.setShipment(shipment);
                    log.debug("Costo de envío: " + shipmentCost);
                } catch (UnirestException ex) {
                    log.error(ex.getMessage());
                    log.error("Error al calcular costo de envío");
                    shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.ERROR_GENERICO);
                    return shipmentDto;
                }
                //shipmentEntityManagerBean.persist(shipment);
                log.info("Envio creado y almacenado correctamente: " + shipment);           
            } else {
                log.error("Ocurrio algun problema al validar los datos del Envio");
                return shipmentDto;
            }
        } catch (PersistenceException e) {
            e.printStackTrace();
            shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.ERROR_BD);
            log.error(e.getMessage());
            return shipmentDto;
        } catch (Exception e) {
            e.printStackTrace();
            shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.ERROR_GENERICO);
            log.error(e.getMessage());
            return shipmentDto;
        }
        shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.OK);
        return shipmentDto;
    }
    
    private ShipmentDto validateClients(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        try {
            shipment.setSenderId(1L);
            HttpResponse<JsonNode> response
                    = Unirest.get("http://localhost:8080/Clients-war/client/findbyid?id=" + shipment.getSenderId()).asJson();
            log.debug("Cliente remitente encontrado para el Envio " + shipment);
        } catch (UnirestException ex) {
            log.error(ex.getMessage());
            log.error("No se encontro cliente remitente para el Envio " + shipment);
            shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.NO_EXISTE_CLIENTE_REMITENTE);
            return shipmentDto;
        }
        try {
            shipment.setReceiverId(2L);
            HttpResponse<JsonNode> response
                    = Unirest.get("http://localhost:8080/Clients-war/client/findbyid?id=" + shipment.getReceiverId()).asJson();
            log.debug("Cliente receptor encontrado para el Envio " + shipment);
        } catch (UnirestException ex) {
            log.error(ex.getMessage());
            log.error("No se encontro cliente receptor para el Envio " + shipment);
            shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.NO_EXISTE_CLIENTE_RECEPTOR);
            return shipmentDto;
        }        
        shipmentDto.setShipmentStatusEnum(ShipmentStatusEnum.OK);
        shipmentDto.setShipment(shipment);
        return shipmentDto;   
    }

    @Override
    public ShipmentDto getClosestCadets(ShipmentDto shipmentDto) {
        ArrayList<ShipmentClosestCadetDto> listCadets = new ArrayList<>();
        String address = shipmentDto.getShipment().getSenderAddress();
        try {
            HttpResponse<JsonNode> response
                    = Unirest.get("http://localhost:8080/Cadets-war/cadet/getclosestcadets?address=" + address).asJson();
            JsonNode json = response.getBody();
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<ShipmentClosestCadetDto>>() { } .getType();
            listCadets = gson.fromJson(json.toString(), listType);
            shipmentDto.setClosestCadets(listCadets);
        } catch (UnirestException ex) {
            log.error(ex.getMessage());            
        }       
        return shipmentDto;
    }
    
    @Override
    public EntityStatusEnum setCadet(Long id, Long cadetId) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            //Shipment shipment = shipmentEntityManagerBean.find(id);
            Shipment shipment = new Shipment();            
            if (shipment != null) {
                if (validateCadet(cadetId)) {
                    shipment.setCadetId(cadetId);                
                    //shipmentEntityManagerBean.merge(shipment);
                    log.info("Cadete almacenado correctamente para el envío: " + shipment);
                    log.debug("Serializando mensaje");
                    String json = new Gson().toJson(shipment);
                    log.debug("Depositando mensaje para Cadete " + shipment);
                    try {
                        HttpResponse<JsonNode> response
                                = Unirest.post("http://localhost:8080/Notifications-war/notification/enqueueforcadet")
                                        .header("body", json).asJson();                    
                    } catch (UnirestException ex) {
                        log.error(ex.getMessage());
                    }
                } else {
                    ret = EntityStatusEnum.NO_EXISTE;
                }                
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }
    
    private boolean validateCadet(Long id) {
        try {
            HttpResponse<JsonNode> response
                    = Unirest.get("http://localhost:8080/Cadets-war/cadet/findbyid?id=" + id).asJson();
            log.debug("Cadete encontrado para el Envio");
            return true;
        } catch (UnirestException ex) {
            log.error(ex.getMessage());
            log.error("No se encontro cadete seleccionado para el Envio");
            return false;
        }        
    }
    
    @Override
    public EntityStatusEnum confirmReception(Long id) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            //Shipment shipment = shipmentEntityManagerBean.find(id);
            Shipment shipment = new Shipment();
            shipment.setReceiverId(1L);
            shipment.setSenderId(2L);
            if (shipment != null) {
                shipment.setReceived(true);
                //shipmentEntityManagerBean.merge(shipment);
                log.info("Envío recibido correctamente: " + shipment);
                log.debug("Serializando mensaje");
                String body = new Gson().toJson(shipment);
                //AGREGAR LINK PARA CALIFICAR
                String emailSender = getClientEmail(shipment.getSenderId());
                String emailReceiver = getClientEmail(shipment.getReceiverId());                
                log.debug("Depositando mensaje para Clientes " + body);
                try {
                    HttpResponse<JsonNode> response
                            = Unirest.post("http://localhost:8080/Notifications-war/notification/enqueueforclients")
                                    .header("emailsender", emailSender)
                                    .header("emailreceiver", emailReceiver)
                                    .header("body", body).asJson();                    
                } catch (UnirestException ex) {
                    log.error(ex.getMessage());
                }             
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }        
        return ret;
    }

    @Override
    public EntityStatusEnum modify(Shipment shipment) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Shipment aux = shipmentEntityManagerBean.find(shipment.getId());
            if (aux != null) {
                aux.setCadetId(shipment.getCadetId());
                aux.setReceiverId(shipment.getReceiverId());
                aux.setSenderId(shipment.getSenderId());
                aux.setDescription(shipment.getDescription());
                aux.setReceiverAddress(shipment.getReceiverAddress());
                aux.setSenderAddress(shipment.getSenderAddress());
                aux.setVehicleId(shipment.getVehicleId());
                shipmentEntityManagerBean.merge(aux);
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public EntityStatusEnum delete(Long id) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Shipment shipment = shipmentEntityManagerBean.find(id);
            if (shipment != null) {
                shipmentEntityManagerBean.remove(shipment);
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error(e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public Shipment findById(Long id) {
        log.debug("Buscando Envio por id " + id);
        try {
            Shipment shipment = shipmentEntityManagerBean.find(id);
            log.debug("Envio encontrado " + shipment.toString());            
            return shipment;
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Envio por id", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Envio por id", e);
            return null;
        }
    }

    @Override
    public ArrayList<Shipment> findBySender(Long clientId) {
        ArrayList<Shipment> ret;
        try {
            List<Shipment> list = shipmentEntityManagerBean.listShipmentsSender(clientId);
            ret = new ArrayList<>(list);         
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Envio por cliente receptor", e);
            ret = new ArrayList<>();
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Envio por cliente remitente", e);
            ret = new ArrayList<>();
        }       
        return ret;
    }

    @Override
    public ArrayList<Shipment> findByReceiver(Long clientId) {
        ArrayList<Shipment> ret;
        try {
            List<Shipment> list = shipmentEntityManagerBean.listShipmentsReceiver(clientId);
            ret = new ArrayList<>(list);         
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Envio por cliente receptor", e);
            ret = new ArrayList<>();
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Envio por cliente receptor", e);
            ret = new ArrayList<>();
        }       
        return ret;
    }
    
    @Override
    public ArrayList<Shipment> listCadetShipments(Long cadetId) {
        ArrayList<Shipment> ret;
        try {
            List<Shipment> list = shipmentEntityManagerBean.listShipmentsCadet(cadetId);
            ret = new ArrayList<>(list);         
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Envio por cadete", e);
            ret = new ArrayList<>();
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Envio por cliente receptor", e);
            ret = new ArrayList<>();
        }       
        return ret;
    }

    private String getClientEmail(Long id) {
        String email = "";
        try {
            HttpResponse<String> response
                    = Unirest.get("http://localhost:8080/Clients-war/client/getemail?id=" + id).asString();
            email = response.getBody();
        } catch (UnirestException ex) {
            log.error(ex.getMessage());
            log.error("Error al obtener email del cliente");
        }
        return email;
    }
    
}