package com.enviosya.shipments.persistence;

import com.enviosya.shipments.entities.Shipment;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diego
 */
@Singleton
@LocalBean
public class ShipmentEntityManagerBean {

    @PersistenceContext
    private EntityManager entityManager;

    public void persist(Shipment shipment) {
        entityManager.persist(shipment);
    }

    public Shipment find(Long id) {
        return entityManager.find(Shipment.class, id);
    }

    public void merge(Shipment shipment) {
        entityManager.merge(shipment);
    }

    public void remove(Shipment shipment) {
        entityManager.remove(shipment);
    }
    
    public List<Shipment> listShipmentsSender(Long clientId) {
        return entityManager.createNamedQuery("listShipmentsSender", Shipment.class)
                    .setParameter("senderid", clientId)
                    .getResultList();
    }
    
    public List<Shipment> listShipmentsReceiver(Long clientId) {
        return entityManager.createNamedQuery("listShipmentsReceiver", Shipment.class)
                    .setParameter("receiverid", clientId)
                    .getResultList();
    }
    
    public List<Shipment> listShipmentsCadet(Long cadetId) {
        return entityManager.createNamedQuery("listShipmentsCadet", Shipment.class)
                    .setParameter("cadetid", cadetId)
                    .getResultList();
    }
    
}
