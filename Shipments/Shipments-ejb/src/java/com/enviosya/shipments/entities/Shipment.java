package com.enviosya.shipments.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author mateo
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "listShipmentsSender", 
                query = "SELECT s FROM Shipment s WHERE s.senderId = :senderid"),
        @NamedQuery(name = "listShipmentsReceiver", 
                query = "SELECT s FROM Shipment s WHERE s.receiverId = :receiverid"),
        @NamedQuery(name = "listShipmentsCadet", 
                query = "SELECT s FROM Shipment s WHERE s.cadetId = :cadetid"),
    })
public class Shipment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String description;

    private Long cadetId;

    private Long senderId;

    private Long receiverId;

    private Long vehicleId;

    private String senderAddress;

    private String receiverAddress;
    
    private String picture;
    
    private Double shippingCost;
    
    private boolean received;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCadetId() {
        return cadetId;
    }

    public void setCadetId(Long cadetId) {
        this.cadetId = cadetId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }    

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(Double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }
    
}