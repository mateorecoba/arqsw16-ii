package com.enviosya.shipments.resources;

import com.enviosya.shipments.beans.ShipmentBean;
import com.google.gson.Gson;
import com.enviosya.shipments.entities.Shipment;
import com.enviosya.shipments.dtos.ShipmentDto;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.enviosya.shipments.utils.ShipmentStatusEnum;
import com.enviosya.util.enums.EntityStatusEnum;
import javax.enterprise.context.RequestScoped;

/**
 * REST Web Service
 *
 * @author mateo
 */
@Path("shipment")
@RequestScoped
public class ShipmentResource {

    private Response response;

    @EJB
    private ShipmentBean shipmentBean;

    public ShipmentResource() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addShipment(String params) {
        Gson gson = new Gson();
        Shipment shipment = gson.fromJson(params, Shipment.class);
        ShipmentDto shipmentDto = shipmentBean.add(shipment);
        if (shipmentDto.getShipmentStatusEnum() == ShipmentStatusEnum.OK) {
            shipmentDto = shipmentBean.getClosestCadets(shipmentDto);
            if (!shipmentDto.getClosestCadets().isEmpty()) {
                response = Response.ok(gson.toJson(shipmentDto)).build();
            } else {
                response = Response.status(Response.Status.NOT_FOUND).build();
            }
        } else if (shipmentDto.getShipmentStatusEnum() == ShipmentStatusEnum.ERROR_BD
                || shipmentDto.getShipmentStatusEnum() == ShipmentStatusEnum.ERROR_GENERICO) {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }

    @PUT
    @Path("setcadet")
    @Produces(MediaType.APPLICATION_JSON)
    public Response setCadet(@QueryParam(value = "id") Long id, @QueryParam(value = "cadetid") Long cadetId) {
        Gson gson = new Gson();
        EntityStatusEnum message = shipmentBean.setCadet(id, cadetId);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @PUT
    @Path("confirmreception")
    @Produces(MediaType.APPLICATION_JSON)
    public Response confirmReception(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        EntityStatusEnum message = shipmentBean.confirmReception(id);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyShipment(String params) {
        Gson gson = new Gson();
        Shipment shipment = gson.fromJson(params, Shipment.class);
        EntityStatusEnum message = shipmentBean.modify(shipment);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteShipment(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        EntityStatusEnum message = shipmentBean.delete(id);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    @GET
    @Path("findbyid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findShipmentById(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        Shipment shipment = shipmentBean.findById(id);
        if (shipment != null) {
            response = Response.ok(gson.toJson(shipment)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();            
        }
        return response;
    }

    @GET
    @Path("findbysender")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findShipmentBySender(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        ArrayList<Shipment> shipmentsList = shipmentBean.findBySender(id);
        if (!shipmentsList.isEmpty()) {
            response = Response.ok(gson.toJson(shipmentsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }
    
    @GET
    @Path("findbyreceiver")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findShipmentByReceiver(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        ArrayList<Shipment> shipmentsList = shipmentBean.findByReceiver(id);
        if (!shipmentsList.isEmpty()) {
            response = Response.ok(gson.toJson(shipmentsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }

    @GET
    @Path("listclientshipments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listClientShipments(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        ArrayList<Shipment> senderList = shipmentBean.findBySender(id);
        ArrayList<Shipment> receiverList = shipmentBean.findByReceiver(id);
        ArrayList<Shipment> shipmentsList = new ArrayList<>();
        shipmentsList.addAll(senderList);
        shipmentsList.addAll(receiverList);
        if (!shipmentsList.isEmpty()) {
            response = Response.ok(gson.toJson(shipmentsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }

    @GET
    @Path("listcadetshipments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listCadetShipments(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        ArrayList<Shipment> shipmentsList = shipmentBean.listCadetShipments(id);
        if (!shipmentsList.isEmpty()) {
            response = Response.ok(gson.toJson(shipmentsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }
        
}
