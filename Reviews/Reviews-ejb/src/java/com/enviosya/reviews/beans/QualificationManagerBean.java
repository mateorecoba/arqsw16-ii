package com.enviosya.reviews.beans;

import com.enviosya.reviews.beans.interfaces.QualificationManager;
import com.enviosya.reviews.qualification.filter.filters.DuplicatedReviewFilter;
import com.enviosya.reviews.qualification.filter.filters.CommentSemanticFilter;
import com.enviosya.reviews.qualification.filter.filters.CommentBadWordsFilter;
import com.enviosya.reviews.qualification.filter.filters.UserValidationFilter;
import com.enviosya.reviews.qualification.filter.filters.CommentLengthFilter;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.persistence.ReviewEntityManagerBean;
import com.enviosya.reviews.qualification.filter.Filter;
import com.enviosya.reviews.qualification.pipe.Pipe;
import com.enviosya.reviews.qualification.pipe.QueuePipe;
import com.enviosya.reviews.qualification.pump.SimplePump;
import com.enviosya.reviews.qualification.pump.Pump;
import com.enviosya.reviews.qualification.sink.ConcreteSink;
import com.enviosya.reviews.qualification.sink.Sink;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

/**
 *
 * @author mateo
 */
@Singleton
@LocalBean
public class QualificationManagerBean implements QualificationManager {

    @EJB
    private ReviewEntityManagerBean reviewEntityManagerBean;
    
    /**
     * receives the reviews and send them to first filter
     */
    private Pipe<Review> pumpOutput = new QueuePipe();
    private Pump pump = new SimplePump(pumpOutput);
    
    /**
     * finallize review
     */
    private Pipe<Review> outputToSink = new QueuePipe();
    private Sink<Review> genericSink = new ConcreteSink(outputToSink, reviewEntityManagerBean);

    /**
     * check if the user reviewing is the same as logged
     */
    private Pipe<Review> userValidatorOutput = new QueuePipe();
    private Filter userValidatorFilter = new UserValidationFilter(pumpOutput, userValidatorOutput, outputToSink);

    /**
     * check if there is no duplicated review
     */
    private Pipe<Review> duplicatedReviewOutput = new QueuePipe();
    private Filter duplicatedReviewFilter = new DuplicatedReviewFilter(
            userValidatorOutput, duplicatedReviewOutput, outputToSink);

    /**
     * check if the comments has valid length
     */
    private Pipe<Review> commentLengthOutput = new QueuePipe();
    private Filter commentLengthFilter = new CommentLengthFilter(
            duplicatedReviewOutput, commentLengthOutput, outputToSink);

    /**
     * check if comments has no bad words
     */
    private Pipe<Review> commentBadWordsOutput = new QueuePipe();
    private Filter commentBadWordsFilter = new CommentBadWordsFilter(
            commentLengthOutput, commentBadWordsOutput, outputToSink);

    /**
     * check if the comments has a good semantic
     */
    private Filter commentSemanticFilter = new CommentSemanticFilter(
            commentBadWordsOutput, outputToSink, outputToSink);
    

    /**
     * initiation of all filters
     */
    public QualificationManagerBean() {
        userValidatorFilter.start();
        duplicatedReviewFilter.start();
        commentLengthFilter.start();
        commentBadWordsFilter.start();
        commentSemanticFilter.start();
        genericSink.start();
    }

    @Override
    public void putReview(Review review) {
        pump.generateInto(review);
    }
}
