package com.enviosya.reviews.beans.interfaces;

import com.enviosya.reviews.domain.entities.Review;
import javax.ejb.Local;

/**
 *
 * @author mateo
 */
@Local
public interface QualificationManager {
    
    void putReview(Review review);
    
}
