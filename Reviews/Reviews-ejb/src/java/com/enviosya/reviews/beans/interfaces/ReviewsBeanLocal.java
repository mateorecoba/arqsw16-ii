package com.enviosya.reviews.beans.interfaces;

import javax.ejb.Local;

/**
 *
 * @author mateo
 */
@Local
public interface ReviewsBeanLocal {
    
    String getCadetsReviews(Long id);
    
}
