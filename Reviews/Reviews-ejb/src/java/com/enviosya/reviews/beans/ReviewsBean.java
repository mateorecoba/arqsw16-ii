package com.enviosya.reviews.beans;

import com.enviosya.reviews.beans.interfaces.ReviewsBeanLocal;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.persistence.ReviewEntityManagerBean;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author mateo
 */
@Stateless
@LocalBean
public class ReviewsBean implements ReviewsBeanLocal {

    @EJB
    private ReviewEntityManagerBean reviewEntityManagerBean;
    
    @Override
    public String getCadetsReviews(Long shipmentId) {
        
        // llamar con UNIREST al modulo de shipment para traer el id del cadete del shipment
        Long cadetId = 1L;
        
        List<Review> list = new ArrayList<>();
        list = reviewEntityManagerBean.findCadetReviews(shipmentId);
        
        list.add(new Review("Comentari", 1, ReviewStatusEnum.PENDING, "www.link.com/asdfasdfsad", 1L));
        list.add(new Review("Comentari", 1, ReviewStatusEnum.PENDING, "www.link.com/asdfasdfsad", 1L));
        list.add(new Review("Comentari", 1, ReviewStatusEnum.PENDING, "www.link.com/asdfasdfsad", 1L));
        list.add(new Review("Comentari", 1, ReviewStatusEnum.PENDING, "www.link.com/asdfasdfsad", 1L));
        list.add(new Review("Comentari", 1, ReviewStatusEnum.PENDING, "www.link.com/asdfasdfsad", 1L));

         
        return "cadetesconrevies";
    }

}
