package com.enviosya.reviews.domain.entities;

import com.enviosya.reviews.enums.ReviewStatusEnum;
import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mateo
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "findByShipmentAndUser",
            query = "SELECT r FROM Review r WHERE r.shipmentId = :shipmentId and r.userIdWhoReviewed = :userId "),
    @NamedQuery(name = "findCadetReviews",
            query = "SELECT r FROM Review r WHERE r.cadetId = :cadetId and r.status = status BY r.creationDate DESC")

    })
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(length = 100)
    private String comment;

    @NotNull
    private Integer score;

    @NotNull
    private ReviewStatusEnum status;

    /**
     * Para guardar el status si fue cambiado manualmente
     */
    private ReviewStatusEnum statusBeforeChanged = ReviewStatusEnum.NOT_SETTED;

    @NotNull
    @Column(length = 100)
    private String link;

    @NotNull
    private Long shipmentId;

    @NotNull
    private Long cadetId;

    @NotNull
    private Long userIdWhoReviewed;

    private Long userIdWhoChangedStatus;

    @Transient
    private String token;
    
    private Date creationDate;

    public Review() {
    }

    public Review(String comment, Integer score, ReviewStatusEnum status, String link, Long shipmentId) {
        this.comment = comment;
        this.score = score;
        this.status = ReviewStatusEnum.RECEIVED;
        this.link = link;
        this.shipmentId = shipmentId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public ReviewStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ReviewStatusEnum status) {
        this.status = status;
    }

    public ReviewStatusEnum getStatusBeforeChanged() {
        return statusBeforeChanged;
    }

    public void setStatusBeforeChanged(ReviewStatusEnum statusBeforeChanged) {
        this.statusBeforeChanged = statusBeforeChanged;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(Long shipmentId) {
        this.shipmentId = shipmentId;
    }

    public Long getCadetId() {
        return cadetId;
    }

    public void setCadetId(Long cadetId) {
        this.cadetId = cadetId;
    }

    public Long getUserIdWhoReviewed() {
        return userIdWhoReviewed;
    }

    public void setUserIdWhoReviewed(Long userIdWhoReviewed) {
        this.userIdWhoReviewed = userIdWhoReviewed;
    }

    public Long getUserIdWhoChangedStatus() {
        return userIdWhoChangedStatus;
    }

    public void setUserIdWhoChangedStatus(Long userIdWhoChangedStatus) {
        this.userIdWhoChangedStatus = userIdWhoChangedStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    
    @Override
    public String toString() {
        return "Review{" + "comment=" + comment + ", score=" + score + ", status=" + status + '}';
    }
}
