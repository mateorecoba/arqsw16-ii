package com.enviosya.reviews.qualification.pipe;

public interface Pipe<T> {
    
    boolean put(T obj);
    
    T pop() throws InterruptedException;
    
}

