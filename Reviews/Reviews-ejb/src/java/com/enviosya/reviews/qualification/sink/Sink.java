package com.enviosya.reviews.qualification.sink;

import com.enviosya.reviews.qualification.pipe.Pipe;

public abstract class Sink<T> extends Thread {
    
    protected Pipe<T> input;

    public Sink(Pipe<T> input) {
        this.input = input;
    }

    @Override
    public void run() {
        takeFrom(input);
    }

    public abstract void takeFrom(Pipe<T> pipe);
}
