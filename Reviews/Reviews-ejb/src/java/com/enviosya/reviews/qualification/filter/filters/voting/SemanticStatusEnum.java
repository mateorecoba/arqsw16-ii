package com.enviosya.reviews.qualification.filter.filters.voting;

/**
 *
 * @author mateo
 */
public enum SemanticStatusEnum {
    POSITIVE, NEGATIVE, NEUTRAL;
    
}
