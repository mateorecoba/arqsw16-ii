package com.enviosya.reviews.qualification.filter.filters.voting;

import com.enviosya.log.logger.CustomLogger;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 *
 * @author mateo
 */
public class JaperkVote implements SemanticVoting {

    CustomLogger log = new CustomLogger(JaperkVote.class);

    @Override
    public SemanticStatusEnum checkSentiment(String comment) {
        
        SemanticStatusEnum status = SemanticStatusEnum.NEUTRAL;
        log.debug("Status before evaluation " + status);

        try {
            HttpResponse<JsonNode> response = Unirest.post("https://japerk-text-processing.p.mashape.com/sentiment/")
                    .header("X-Mashape-Key", "5pDSk431v1mshAIxehM6IDLdC7y4p1DRKrejsnXxLit4B5hNMN")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Accept", "application/json")
                    .field("language", "english")
                    .field("text", comment)
                    .asJson();

            /*
            "probability": {"neg": 0.52276260516530182, "neutral": 0.73468869474151877, "pos": 0.47723739483469824}, 
                            "label": "neutral"}"
            {"label": "pos", "probability": { "neg": 0.39680317, "neutral": 0.28207585, "pos": 0.60319686 }
            }
             */
            log.debug("Response received: " + response.getBody().getObject());
            String value = response.getBody().getObject().get("label").toString();

            if (value.equalsIgnoreCase("pos")) {
                status = SemanticStatusEnum.POSITIVE;
            } else if (value.equalsIgnoreCase("neg")) {
                status = SemanticStatusEnum.NEGATIVE;
            }

        } catch (UnirestException ex) {
            log.error(ex);
        }

        log.debug("Status after evaluation " + status);
        return status;
    }

}
