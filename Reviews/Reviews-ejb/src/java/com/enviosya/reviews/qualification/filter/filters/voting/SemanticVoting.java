package com.enviosya.reviews.qualification.filter.filters.voting;

/**
 *
 * @author mateo
 */
public interface SemanticVoting {
    
    SemanticStatusEnum checkSentiment(String comment);
}
