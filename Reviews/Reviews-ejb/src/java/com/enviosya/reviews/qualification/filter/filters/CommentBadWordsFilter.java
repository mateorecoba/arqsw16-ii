package com.enviosya.reviews.qualification.filter.filters;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.qualification.filter.SimpleFilter;
import com.enviosya.reviews.qualification.pipe.Pipe;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class CommentBadWordsFilter extends SimpleFilter<Review, Review> {

    private CustomLogger log = new CustomLogger(CommentBadWordsFilter.class);

    private Set<String> badWords;

    private final String badWordsPath = System.getProperty("user.dir") + "/bad_words.txt";
            //"../com.enviosya.reviews.files/bad_words.txt";

    public CommentBadWordsFilter(Pipe<Review> input, Pipe<Review> output, Pipe<Review> outputToSink) {
        super(input, output, outputToSink);
        loadBadWords();
    }

    @Override
    protected Review transformOne(Review in) {
        if (hasBadWords(in)) {
            log.debug("The comment has bad words " + in.toString());
            in.setStatus(ReviewStatusEnum.INVALID_COMMENT_BAD_WORDS);
            //this step allways send to next filter
        }
        return in;
    }

    private boolean hasBadWords(Review in) {
        log.debug("Checking bad words of " + in.toString());
        boolean hasBadWords = false;
        String[] words = in.getComment().split(" ");

        for (String commentWord : words) {
            if (this.badWords.contains(commentWord)) {
                hasBadWords = true;
                break;
            }
        }
        return hasBadWords;
    }

    private void loadBadWords() {
        log.debug("Loading bad words from " + badWordsPath);
        this.badWords = new HashSet();
        
        try {
            InputStream is = new FileInputStream(badWordsPath);
            if (is == null) {
                log.debug("The file not exists in path " + badWordsPath);
            } else {
                String line;
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                int aux = 1;
                while ((line = br.readLine()) != null) {
                    if (!line.isEmpty()) {
                        this.badWords.add(line);
                        log.debug(aux++ + " Bad Word loaded: " + line);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error loading bad words from " + badWordsPath, e);
        }
    }
}
