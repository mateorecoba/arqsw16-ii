package com.enviosya.reviews.qualification.filter;

import com.enviosya.reviews.qualification.pipe.Pipe;

public abstract class Filter<I, O> extends Thread {

    protected Pipe<I> input;

    protected Pipe<O> output;
    
    public Filter(Pipe<I> input, Pipe<O> output) {
        this.input = input;
        this.output = output;
    }

    @Override
    public void run() {
        transformBetween(input, output);
    }

    protected abstract void transformBetween(Pipe<I> input, Pipe<O> output);
}