package com.enviosya.reviews.qualification.filter;

import com.enviosya.reviews.qualification.pipe.Pipe;

public abstract class SimpleFilter<I, O> extends Filter<I, O> {

    
    protected Pipe<O> outputToSink;

    public SimpleFilter(Pipe<I> input, Pipe<O> output, Pipe<O> outputToSink) {
        super(input, output);
        this.outputToSink = outputToSink;
    }

    @Override
    protected void transformBetween(Pipe<I> input, Pipe<O> output) {
        try {
            I in;
            while ((in = input.pop()) != null) {
                O out = transformOne(in);
                output.put(out);
            }
        } catch (InterruptedException e) {
            // TODO handle properly, using advice in http://www.ibm.com/developerworks/java/library/j-jtp05236/
            System.err.println("interrupted");
            e.printStackTrace();
            return;
        }
    }

    protected abstract O transformOne(I in);
    
    protected void sendToSink(O in) {
        this.outputToSink.put(in);
    }
    
}
