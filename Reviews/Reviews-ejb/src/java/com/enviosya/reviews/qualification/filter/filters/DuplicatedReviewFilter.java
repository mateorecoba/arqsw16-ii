package com.enviosya.reviews.qualification.filter.filters;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.qualification.filter.SimpleFilter;
import com.enviosya.reviews.qualification.pipe.Pipe;
import java.util.ArrayList;
import java.util.List;

public class DuplicatedReviewFilter extends SimpleFilter<Review, Review> {

    private CustomLogger log = new CustomLogger(DuplicatedReviewFilter.class);
    
    public DuplicatedReviewFilter(Pipe<Review> input, Pipe<Review> output, Pipe<Review> outputToSink) {
        super(input, output, outputToSink);
    }

    @Override
    protected Review transformOne(Review in) {
        if (isDuplicated(in)) {
            log.debug("Review duplicated found for " + in.toString());
            in.setStatus(ReviewStatusEnum.DUPLICATED);
            sendToSink(in);
            return null;
        }
        return in;
    }

    private boolean isDuplicated(Review in) {
        log.debug("Looking for duplicated review " + in.toString());
        boolean isDuplicated = true;

        List<Review> list = new ArrayList<>();
        isDuplicated = !list.isEmpty();

        if (isDuplicated) {
            log.debug("List of Reviews found for " + in.toString());
            for (Review review : list) {
                log.debug(review.toString());
            }
        }

        return isDuplicated;
    }
}
