package com.enviosya.reviews.qualification.pipe;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

// replaceable with BlockingQueue<T>? That doesn't support closing, though
public class QueuePipe<T> implements Pipe<T> {

    private Queue<T> buffer = new LinkedList<T>();
    private boolean hasReadLastObject = false;

    @Override
    public synchronized boolean put(T obj) {
        boolean wasAdded = false;
        if (obj != null) {
            wasAdded = buffer.add(obj);
            notify();
            //System.out.println("added to pipe: " + (obj==null?"<null>":obj.toString()));

        }
        return wasAdded;
    }

    @Override
    // not using next() and willHaveNext() because a currently-empty pipe might be
    //  closed after the willHaveNext() check, causing next() to wait forever
    // not using an exception because would require consumers to write unidiomatic `while(true)`
    // not using an Option because there is no standard Option and reimplementing it is too annoying
    public synchronized T pop() throws InterruptedException {
        if (hasReadLastObject) {
            throw new NoSuchElementException("pipe is closed and empty; will never contain any further values");
        }

        while (buffer.isEmpty()) {
            //pipe empty - wait
            wait();
        }

        T obj = buffer.remove();
        if (obj == null) { 
            //will be null if it's the last element
            hasReadLastObject = true;
        }
        return obj;
    }
}
