package com.enviosya.reviews.qualification.filter.filters;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.qualification.filter.SimpleFilter;
import com.enviosya.reviews.qualification.pipe.Pipe;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class UserValidationFilter extends SimpleFilter<Review, Review> {

    private CustomLogger log = new CustomLogger(UserValidationFilter.class);

    public UserValidationFilter(Pipe<Review> input, Pipe<Review> output, Pipe<Review> outputToSink) {
        super(input, output, outputToSink);
    }

    @Override
    protected Review transformOne(Review in) {
        log.debug("Checking review " + in.toString());
        if (!isUserOk(in)) {
            log.debug("Review " + in.toString() + " has not a valid user");
            in.setStatus(ReviewStatusEnum.INVALID_USER);
            sendToSink(in);
            return null;
        }
        return in;
    }

    private boolean isUserOk(Review review) {
        log.debug("Checking Logged User for review " + review.toString());
        boolean isValidUser = false;

        try {
            Long clientId = getClientByToken(review.getToken());
            if (checkShipmentClients(clientId, review.getShipmentId())) {
                review.setUserIdWhoReviewed(clientId);
                review.setShipmentId(clientId);
                isValidUser = true;
            }
        } catch (Exception ex) {
            log.error("Error trying to load Client and Shipment for the review", ex);
        }

        return isValidUser;
    }

    private Long getClientByToken(String token) {
        log.debug("Calling to Auth WS to get client id by token");
        Long clientId = -1L;

        try {
            //antes, utilizabamos el bean Client client = authManagerBean.getClientByToken(review.getToken());
            HttpResponse<String> response
                    = Unirest.get("http://localhost:8080/Authentication-war/authentication/getClientByToken/?token=" + token)
                    .asString();

            if (response.getStatus() == 200) {
                clientId = Long.valueOf(response.getBody());
                log.debug("Client found by token " + clientId);
            } else {
                log.debug("Client not found by token. Response status is " + response.getStatus());
            }
        } catch (UnirestException ex) {
            log.error(ex);
        }

        return clientId;
    }

    private boolean checkShipmentClients(Long clientId, Long shipmentId) {
        boolean isValidUser = false;
        try {
            //antes, utilizabamos el Bean Shipment shipment = shipmentBean.findById(review.getShipmentId());
            HttpResponse<JsonNode> response
                    = Unirest.get("http://localhost:8080/Shipments-war/shipment/findbyid?id=" + shipmentId)
                    .asJson();

            if (response.getStatus() == 200) {
                log.debug("Shipment found by id " + shipmentId);

                Long senderId = response.getBody().getObject().getLong("senderId");
                Long receiverId = response.getBody().getObject().getLong("receiverId");

                if (clientId == senderId || clientId == receiverId) {
                    isValidUser = true;
                }
            } else {
                log.debug("Shipment not found by id. Response status is " + response.getStatus());
            }
        } catch (UnirestException ex) {
            log.error(ex);
        }

        return isValidUser;

    }

}
