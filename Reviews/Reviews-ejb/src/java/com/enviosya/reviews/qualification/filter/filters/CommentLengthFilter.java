package com.enviosya.reviews.qualification.filter.filters;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.qualification.filter.SimpleFilter;
import com.enviosya.reviews.qualification.pipe.Pipe;

public class CommentLengthFilter extends SimpleFilter<Review, Review> {

    private final Integer minLenght = 5;
    private CustomLogger log = new CustomLogger(CommentLengthFilter.class);

    public CommentLengthFilter(Pipe<Review> input, Pipe<Review> output, Pipe<Review> outputToSink) {
        super(input, output, outputToSink);
    }

    @Override
    protected Review transformOne(Review in) {
        log.debug("Checking comment length of " + in.toString());
        if (in.getComment().length() < minLenght) {
            log.error("Comment too short " + in.toString());
            in.setStatus(ReviewStatusEnum.INVALID_COMMENT_LENGTH);
            sendToSink(in);
            return null;
        }        
        return in;
    }
}
