package com.enviosya.reviews.qualification.filter.filters;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.qualification.filter.SimpleFilter;
import com.enviosya.reviews.qualification.filter.filters.voting.CilenisapiVote;
import com.enviosya.reviews.qualification.filter.filters.voting.JaperkVote;
import com.enviosya.reviews.qualification.filter.filters.voting.SemanticStatusEnum;
import com.enviosya.reviews.qualification.filter.filters.voting.SemanticVoting;
import com.enviosya.reviews.qualification.pipe.Pipe;

public class CommentSemanticFilter extends SimpleFilter<Review, Review> {

    private CustomLogger log = new CustomLogger(CommentSemanticFilter.class);

    public CommentSemanticFilter(Pipe<Review> input, Pipe<Review> output, Pipe<Review> outputToSink) {
        super(input, output, outputToSink);
    }

    @Override
    protected Review transformOne(Review in) {
        log.debug("CommentSemanticFilter filtered " + in.toString());

        if (checkSemantic(in)) {
            in.setStatus(ReviewStatusEnum.APPROVED);
        } else {
            in.setStatus(ReviewStatusEnum.INVALID_COMMENT_SEMMANTIC);
            sendToSink(in);
            return null;
        }

        return in;
    }

    private boolean checkSemantic(Review in) {
        log.debug("Checking semantic of comment");
        boolean isSentimentOk = false;

        SemanticStatusEnum[] status = new SemanticStatusEnum[2];
        SemanticVoting cilenisapiVote = new CilenisapiVote();
        SemanticVoting japerkVoting = new JaperkVote();

        status[0] = cilenisapiVote.checkSentiment(in.getComment());
        status[1] = japerkVoting.checkSentiment(in.getComment());

        SemanticStatusEnum result = analizeVotes(status);

        if (result != SemanticStatusEnum.NEGATIVE) {
            isSentimentOk = true;
        }

        return isSentimentOk;
    }

    private SemanticStatusEnum analizeVotes(SemanticStatusEnum[] status) {
        log.debug("Analizing votes of semmantic");
        SemanticStatusEnum result = SemanticStatusEnum.NEUTRAL;
        int totalVotes = status.length;
        int positiveVotes = 0;
        double minRelation = 0.5;

        for (int i = 0; i < status.length; i++) {
            log.debug(i + " Vote is " + status[i]);
            if (status[i] == SemanticStatusEnum.NEGATIVE) {
                result = SemanticStatusEnum.NEGATIVE;
            }

            if (status[i] == SemanticStatusEnum.POSITIVE) {
                positiveVotes++;
            }
        }

        //if there are not negative votes, maybe could be POSITIVE
        if (result != SemanticStatusEnum.NEGATIVE) {
            // if more than the half are POSITIVE, is POSITIVE
            if (positiveVotes / totalVotes > minRelation) {
                log.debug("Relation of positive votes is " + positiveVotes / totalVotes);
                result = SemanticStatusEnum.POSITIVE;
            }
        }

        log.debug("Final vote is " + result);
        return result;
    }
}
