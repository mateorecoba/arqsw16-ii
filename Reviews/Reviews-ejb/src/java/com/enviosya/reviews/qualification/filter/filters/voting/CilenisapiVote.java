package com.enviosya.reviews.qualification.filter.filters.voting;

import com.enviosya.log.logger.CustomLogger;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

/**
 *
 * @author mateo
 */
public class CilenisapiVote implements SemanticVoting {

    CustomLogger log = new CustomLogger(CilenisapiVote.class);

    @Override
    public SemanticStatusEnum checkSentiment(String comment) {
        SemanticStatusEnum status = SemanticStatusEnum.NEUTRAL;
        log.debug("Status before evaluation " + status);

        comment = comment.replace(" ", "+");

        try {
            HttpResponse<JsonNode> response
                    = Unirest.get("https://cilenisapi.p.mashape.com/sentiment_analyzer?lang_input=en&text=" + comment)
                    .header("X-Mashape-Key", "5pDSk431v1mshAIxehM6IDLdC7y4p1DRKrejsnXxLit4B5hNMN")
                    .header("Accept", "text/plain").asJson();

            // Example of response received {"polarity":{"polarity_weight":0,"polarity_name":"NONE"}}
            log.debug("Response received: " + response.getBody().getObject());
            JSONObject polarity = (JSONObject) response.getBody().getObject().get("polarity");
            String value = polarity.get("polarity_name").toString();

            if (value.equals("POSITIVE")) {
                status = SemanticStatusEnum.POSITIVE;
            } else if (value.equals("NEGATIVE")) {
                status = SemanticStatusEnum.NEGATIVE;
            }
        } catch (UnirestException ex) {
            log.error(ex);
        }

        log.debug("Status after evaluation " + status);
        return status;
    }

}
