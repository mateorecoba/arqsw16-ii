package com.enviosya.reviews.qualification.pump;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.enviosya.reviews.qualification.pipe.Pipe;

public class SimplePump extends Pump<Review> {

    private CustomLogger log = new CustomLogger(SimplePump.class);

    public SimplePump(Pipe<Review> output) {
        super(output);
    }

    @Override
    public void generateInto(Review in) {
        log.debug("Pump generating " + in.toString());
        in.setStatus(ReviewStatusEnum.PENDING);
        log.debug("Review changed to PENDING status" + in.toString());
        output.put(in);
        log.debug("Pump generator finished with " + in.toString());
    }
}
