package com.enviosya.reviews.qualification.sink;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.persistence.ReviewEntityManagerBean;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.qualification.pipe.Pipe;

public class ConcreteSink extends Sink<Review> {

    private ReviewEntityManagerBean reviewEntityManagerBean;
            
    private CustomLogger log = new CustomLogger(ConcreteSink.class);

    public ConcreteSink(Pipe<Review> input, ReviewEntityManagerBean revEntityManagerBean) {
        super(input);
        reviewEntityManagerBean = revEntityManagerBean;
    }

    @Override
    public void takeFrom(Pipe<Review> pipe) {
        try {
            Review in;
            while ((in = pipe.pop()) != null) {
//                TODO confirmar la review

                reviewEntityManagerBean.saveReview(in);
                log.debug("sink finishing " + in.toString());
            }
            log.debug("sink finished");
        } catch (InterruptedException e) {
            log.error(e);
        } finally {
            System.out.close();
        }
    }
}
