package com.enviosya.reviews.qualification.pump;

import com.enviosya.reviews.qualification.pipe.Pipe;

public abstract class Pump<T> {
    
    protected Pipe<T> output;

    public Pump(Pipe<T> output) {
        this.output = output;
    }

    public abstract void generateInto(T var);
    
}