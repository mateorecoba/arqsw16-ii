package com.enviosya.reviews.persistence;

import com.enviosya.log.logger.CustomLogger;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mateo
 */
@Singleton
@LocalBean
public class ReviewEntityManagerBean {

    CustomLogger log = new CustomLogger(ReviewEntityManagerBean.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Review saveReview(Review review) {
        // TODO cambiar cuando funcione entityManager
        if (entityManager != null) {
            entityManager.persist(review);
        }
        log.debug("Saving " + review.toString());
        return review;
    }

    public List<Review> findByShipmentAndUser(Review review) {
        List<Review> list;

        // TODO cambiar cuando funcione entityManager
        if (entityManager != null) {
            list = entityManager.createNamedQuery("findByShipmentAndUser", Review.class)
                    .setParameter("shipmentId", review.getShipmentId())
                    .setParameter("userId", review.getUserIdWhoReviewed())
                    .getResultList();

        } else {
            list = new ArrayList();
        }
        
        return list;
    }

    public List<Review> findCadetReviews(Long id) {
        List<Review> list;

        // TODO cambiar cuando funcione entityManager
        if (entityManager != null) {
            list = entityManager.createNamedQuery("findCadetReviews", Review.class)
                    .setParameter("cadetId", id)
                    .setParameter("status", ReviewStatusEnum.APPROVED)
                    .getResultList();
        } else {
            list = new ArrayList();
        }

        return list;
    }

}
