package com.enviosya.reviews.enums;

/**
 *
 * @author mateo
 */
public enum ReviewStatusEnum {
    RECEIVED("The Review has been recieved."), 
    COMMENT_EMPTY("The comment can not be empty."),
    INVALID_MIN_MAX_SCORE("The score is invalid."),
    PENDING("The Review is Pending."),
    INVALID_USER("The User is Invalid. The shipment user is not logged user."),
    DUPLICATED("The review is duplicated. Shipment already has review."),
    INVALID_COMMENT_LENGTH("The comment is too short."),
    INVALID_COMMENT_BAD_WORDS("The comment has not allowed words (offensive lenguage)."),
    INVALID_COMMENT_SEMMANTIC("The semmantic of the comment is not valid."),
    APPROVED("The review is apptoved."),
    REJECTED("The review has been rejected."),
    MANUAL_REJECTED("The review has been manually rejected by an Admin user."),
    NOT_SETTED("The status is null or was never setted");  
    
    public String message;
    
    ReviewStatusEnum(String message) {
        this.message = message;
    }
}
