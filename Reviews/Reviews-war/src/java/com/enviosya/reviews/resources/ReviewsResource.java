package com.enviosya.reviews.resources;

import com.enviosya.log.logger.CustomLogger;
import com.enviosya.reviews.beans.QualificationManagerBean;
import com.enviosya.reviews.domain.entities.Review;
import com.enviosya.reviews.enums.ReviewStatusEnum;
import com.google.gson.Gson;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author mateo
 */
@Path("reviews")
public class ReviewsResource {

    @Context
    private UriInfo context;

    private final String errorStatus = "ERROR_STATUS";
    private final String errorMessage = "ERROR_MESSAGE";

    @EJB
    private QualificationManagerBean qualificationManagerBean;

    private CustomLogger log = new CustomLogger(ReviewsResource.class);

    /**
     * Creates a new instance of ReviewsResource
     */
    public ReviewsResource() {

    }

    /**
     * PUT method for updating or creating an instance of ReviewsResource
     *
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addReview(String reviewInJson) {
        Gson gson = new Gson();
        Response response = null;
        Review review = gson.fromJson(reviewInJson, Review.class);
        
        if (review != null) {

            log.debug("putting review");

            if (review.getComment().isEmpty()) {
                response = Response.status(Response.Status.BAD_REQUEST)
                        .header(errorStatus, "Request rejected, comment is empty.")
                        .build();

                log.debug("Request rejected, comment is empty.");
            }

            if (review.getScore() > 5 || review.getScore() < 1) {
                response = Response.status(Response.Status.BAD_REQUEST)
                        .header(errorStatus, "Request rejected, score " + review.getScore() + " is not allowed")
                        .build();

                log.debug("Request rejected, score " + review.getScore() + " is not allowed");
            }

            qualificationManagerBean.putReview(review);
            
            if (review.getStatus() != ReviewStatusEnum.APPROVED) {
                response = Response.status(Response.Status.BAD_REQUEST)
                        .header(errorStatus, review.getStatus())
                        .header(errorMessage, review.getStatus().message)
                        .build();
            } else {
                response = Response.status(Response.Status.OK).build();
            }

        }
        return response;
    }
}
