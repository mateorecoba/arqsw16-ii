package com.enviosya.costcalculation.domain;

/**
 *
 * @author Diego
 */
public class Dimensions {
    
    private Double height;
    private Double length;
    private Double weight;

    public Dimensions() {
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }   
    
}