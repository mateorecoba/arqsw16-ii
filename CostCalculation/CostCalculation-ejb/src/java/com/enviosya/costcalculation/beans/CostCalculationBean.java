package com.enviosya.costcalculation.beans;

import com.enviosya.costcalculation.domain.Dimensions;
import com.enviosya.log.logger.CustomLogger;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author Diego
 */
@Stateless
@LocalBean
public class CostCalculationBean implements CostCalculationBeanLocal {

    private CustomLogger log = new CustomLogger(CostCalculationBean.class);
    
    public CostCalculationBean() {
    }
    
    @Override
    public Double getCost(String picture) {
        Double ret = 0.0;
        try {
            HttpResponse<JsonNode> response = Unirest.post("https://mathifonseca-ort-arqsoft-sizer-v1.p.mashape.com/dimensions")
                    .header("X-Mashape-Key", "XjPKzRexaPmshXUxy03uHOH2XzDgp1vCtTCjsnZ3sTrWgZ61W9")
                    .header("Content-Type", "application/json")
                    .header("Accept", "application/json")
                    .body("{\"image\":\"" + picture + "\"}")
                    .asJson();
            JsonNode json = response.getBody();
            Gson gson = new Gson();
            Dimensions dimensions = gson.fromJson(json.toString(), Dimensions.class);
            ret = getCostByDimensions(dimensions.getHeight(), dimensions.getLength(), dimensions.getWeight());
        } catch (UnirestException ex) {
            log.error(ex.getMessage());
        }    
        return ret;
    }

    private Double getCostByDimensions(Double height, Double length, Double weight) {
        //Se define un criterio para calcular el costo
        return height + length + weight;
    }
    
}