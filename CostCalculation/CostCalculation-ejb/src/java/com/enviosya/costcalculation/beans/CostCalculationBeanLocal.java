package com.enviosya.costcalculation.beans;

import javax.ejb.Local;

/**
 *
 * @author Diego
 */
@Local
public interface CostCalculationBeanLocal {
    
    Double getCost(String picture);
    
}
