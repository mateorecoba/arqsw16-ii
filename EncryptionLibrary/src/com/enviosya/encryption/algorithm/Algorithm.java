package com.enviosya.encryption.algorithm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

/**
 *
 * @author mateo
 */
public interface Algorithm {

    String encrypt(String string) throws GeneralSecurityException, UnsupportedEncodingException;
    
    String decrypt(String string) throws GeneralSecurityException, IOException;
    
}
