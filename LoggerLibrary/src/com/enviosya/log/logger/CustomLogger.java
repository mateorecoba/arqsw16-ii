package com.enviosya.log.logger;

import org.apache.log4j.Logger;

/**
 *
 * @author mateo
 */
public class CustomLogger {

    private Logger log;

    public CustomLogger(Class clazz) {
        log = Logger.getLogger(clazz);
    }

    public void error(String message) {
        log.error(message);
    }
    
    public void error(Exception exc) {
        log.error(exc);
    }
    
    public void error(String message, Exception exc) {
        log.error(message, exc);
    }

    public void info(String message) {
        log.info(message);
    }

    public void debug(String message) {
        log.debug(message);
    }

    public void audit(String message) {
        //logear a otro archivo

        //llama a debug
        this.debug(message);
    }

}