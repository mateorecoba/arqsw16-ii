package com.enviosya.clients.resources;

import com.enviosya.clients.beans.ClientBean;
import com.google.gson.Gson;
import com.enviosya.clients.entities.Client;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.enviosya.util.enums.EntityStatusEnum;

/**
 * REST Web Service
 *
 * @author mateo
 */
@Path("client")
public class ClientResource {

    @Context
    private UriInfo context;

    private Response response;

    @EJB
    private ClientBean clientBean;

    public ClientResource() {
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addClient(String params) {
        Gson gson = new Gson();
        Client cliente = gson.fromJson(params, Client.class);
        cliente = clientBean.add(cliente);
        if (cliente != null) {
            response = Response.ok(gson.toJson(cliente)).build();
        } else {
            response = Response.serverError().build();
        }        
        return response;
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyClient(String params) {
        Gson gson = new Gson();
        Client cliente = gson.fromJson(params, Client.class);
        EntityStatusEnum message = clientBean.modify(cliente);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteClient(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        EntityStatusEnum message = clientBean.delete(id);
        if (message == EntityStatusEnum.OK) {
            response = Response.ok(gson.toJson(message)).build();
        } else if (message == EntityStatusEnum.NO_EXISTE) {
            response = Response.status(Response.Status.NOT_FOUND).build();
        } else {
            response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
    
    @GET
    @Path("findbyid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findClientById(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        Client client = clientBean.findById(id);
        if (client != null) {
            response = Response.ok(gson.toJson(client)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }
    
    @GET
    @Path("findbyci")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findClientByCi(@QueryParam(value = "ci") Integer ci) {
        Gson gson = new Gson();
        Client client = clientBean.findByCi(ci);
        if (client != null) {
            response = Response.ok(gson.toJson(client)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }      
        return response;
    }
    
    @GET
    @Path("findbyname")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findClientByName(@QueryParam(value = "firstname") String firstName, 
            @QueryParam(value = "surename") String sureName) {
        Gson gson = new Gson();
        ArrayList<Client> clientsList = clientBean.findByName(firstName, sureName);
        if (!clientsList.isEmpty()) {
            response = Response.ok(gson.toJson(clientsList)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }
    
    @GET
    @Path("getemail")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEmail(@QueryParam(value = "id") Long id) {
        Gson gson = new Gson();
        String email = clientBean.getEmail(id);
        if (email != "") {
            response = Response.ok(gson.toJson(email)).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }        
        return response;
    }
}
