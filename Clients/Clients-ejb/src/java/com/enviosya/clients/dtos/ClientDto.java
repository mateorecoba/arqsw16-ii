package com.enviosya.clients.dtos;

import com.enviosya.util.enums.EntityStatusEnum;
import com.enviosya.clients.entities.Client;
        
/**
 *
 * @author mateo
 */
public class ClientDto {
    
    private Client client;
    private EntityStatusEnum entityManagerStatus;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public EntityStatusEnum getEntityManagerStatus() {
        return entityManagerStatus;
    }

    public void setEntityManagerStatus(EntityStatusEnum entityManagerStatus) {
        this.entityManagerStatus = entityManagerStatus;
    }
        
}