package com.enviosya.clients.persistence;

import com.enviosya.clients.entities.Client;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Diego
 */
@Singleton
@LocalBean
public class ClientEntityManagerBean {

    @PersistenceContext
    private EntityManager entityManager;

    public void persist(Client client) {
        entityManager.persist(client);
    }

    public Client find(Long id) {
        return entityManager.find(Client.class, id);
    }

    public void merge(Client client) {
        entityManager.merge(client);
    }

    public void remove(Client client) {
        entityManager.remove(client);
    }
    
    public Client findClientByCi(Integer ci) {
        return entityManager.createNamedQuery("findClientByCi", Client.class)
                    .setParameter("ci", ci)
                    .getSingleResult();
    }
    
    public List<Client> findClientByName(String firstName, String sureName) {
        return entityManager.createNamedQuery("findClientByName", Client.class)
                    .setParameter("firstname", firstName)
                    .setParameter("surename", sureName)
                    .getResultList();
    }
}
