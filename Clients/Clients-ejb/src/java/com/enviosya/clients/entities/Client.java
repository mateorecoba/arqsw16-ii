package com.enviosya.clients.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mateo
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "findClientByCi", 
                    query = "SELECT c FROM Client c WHERE c.ci = :ci"),
        @NamedQuery(name = "findClientByName", 
                    query = "SELECT c FROM Client c WHERE c.firstName LIKE :firstname and c.sureName LIKE :surename")
    })
public class Client {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(unique = true)
    private Integer ci;

    @NotNull
    @Column(length = 100)
    private String firstName;

    @Column(length = 100)
    private String sureName;

    @Column(length = 100)
    private String email;

    public Client(Long id, Integer ci, String firstName, String sureName, String email) {
        this.id = id;
        this.ci = ci;
        this.firstName = firstName;
        this.sureName = sureName;
        this.email = email;
    }

    public Client(Integer ci, String firstName, String sureName, String email) {
        this.ci = ci;
        this.firstName = firstName;
        this.sureName = sureName;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCi() {
        return ci;
    }

    public void setCi(Integer ci) {
        this.ci = ci;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
}