package com.enviosya.clients.beans;

import com.enviosya.clients.entities.Client;
import com.enviosya.clients.persistence.ClientEntityManagerBean;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import com.enviosya.log.logger.CustomLogger;
import com.enviosya.util.enums.EntityStatusEnum;
import javax.ejb.EJB;
import javax.ejb.LocalBean;

/**
 *
 * @author mateo
 */
@Stateless
@LocalBean
public class ClientBean implements ClientBeanLocal {

    @EJB
    private ClientEntityManagerBean clientEntityManagerBean;

    private CustomLogger log = new CustomLogger(ClientBean.class);

    @Override
    public Client add(Client client) {
        try {
            clientEntityManagerBean.persist(client);
            log.info("Cliente " + client.getId() + " creado correctamente");
        } catch (PersistenceException e) {
            log.error(e);
            return null;
        } catch (Exception e) {
            log.error(e);
            return null;
        }
        return client;
    }

    @Override
    public EntityStatusEnum modify(Client client) {
        EntityStatusEnum retorno = EntityStatusEnum.OK;
        try {
            Client aux = clientEntityManagerBean.find(client.getId());
            if (aux != null) {
                aux.setCi(client.getCi());
                aux.setFirstName(client.getFirstName());
                aux.setSureName(client.getSureName());
                aux.setEmail(client.getEmail());
                clientEntityManagerBean.merge(aux);
                log.info("Cliente " + aux.getId() + " modificado correctamente");
            } else {
                retorno = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al modificar Cliente", e);
            retorno = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al modificar Cliente", e);
            retorno = EntityStatusEnum.ERROR_GENERICO;
        }
        return retorno;
    }

    @Override
    public EntityStatusEnum delete(Long id) {
        EntityStatusEnum ret = EntityStatusEnum.OK;
        try {
            Client client = clientEntityManagerBean.find(id);
            if (client != null) {
                clientEntityManagerBean.remove(client);
                log.info("Cliente " + client.getId() + " eliminado correctamente");
            } else {
                ret = EntityStatusEnum.NO_EXISTE;
            }
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al eliminar Cliente", e);
            ret = EntityStatusEnum.ERROR_BD;
        } catch (Exception e) {
            log.error("Excepcion generica al eliminar Cliente", e);
            ret = EntityStatusEnum.ERROR_GENERICO;
        }
        return ret;
    }

    @Override
    public Client findById(Long id) {
        log.debug("Buscando Cliente por id " + id);
        try {
            //Client client = clientEntityManagerBean.find(id);
            Client client = new Client(1L, 1, "a", "a", "a");
            return client;
            //return null;
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Cliente por id", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Cliente por id", e);
            return null;            
        }
    }

    @Override
    public Client findByCi(Integer ci) {
        try {
            Client client = clientEntityManagerBean.findClientByCi(ci);
            return client;
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Cliente por CI", e);
            return null;
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Cliente por CI", e);
            return null;
        }
    }

    @Override
    public ArrayList<Client> findByName(String firstName, String sureName) {
        ArrayList<Client> ret;
        try {
            List<Client> list = clientEntityManagerBean.findClientByName(firstName, sureName);
            ret = new ArrayList<>(list);
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Cliente por nombre", e);
            ret = new ArrayList<>();
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Cliente por nombre", e);
            ret = new ArrayList<>();
        }
        return ret;
    }
    
    @Override
    public String getEmail(Long id) {
        String ret = "";
        log.debug("Buscando Email Cliente por id " + id);
        try {
            Client client = findById(id);
            if (client != null) {
                ret = client.getEmail();
            }            
        } catch (PersistenceException e) {
            log.error("Excepcion de persistencia al buscar Email Cliente", e);            
        } catch (Exception e) {
            log.error("Excepcion generica al buscar Email Cliente", e);
        }
        return ret;
    }

}