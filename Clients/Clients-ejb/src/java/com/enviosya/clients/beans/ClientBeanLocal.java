package com.enviosya.clients.beans;

import com.enviosya.clients.entities.Client;
import java.util.ArrayList;
import com.enviosya.util.enums.EntityStatusEnum;
import javax.ejb.Local;

/**
 *
 * @author mateo
 */
@Local
public interface ClientBeanLocal {

    Client add(Client client);

    EntityStatusEnum modify(Client client);

    EntityStatusEnum delete(Long id);

    Client findById(Long id);

    Client findByCi(Integer ci);

    ArrayList<Client> findByName(String firstName, String sureName);
    
    String getEmail(Long id);
}
