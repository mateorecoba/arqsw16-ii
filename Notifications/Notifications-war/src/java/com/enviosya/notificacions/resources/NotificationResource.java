package com.enviosya.notificacions.resources;

import com.enviosya.notifications.beans.MessagesAdministratorBean;
import javax.ejb.EJB;
import javax.ws.rs.core.Context;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Diego
 */
@Path("notification")
@RequestScoped
public class NotificationResource {

    private Response response;
    
    @EJB
    private MessagesAdministratorBean messagesAdministratorBean;
    
    public NotificationResource() {
    }
    
    @POST
    @Path("enqueueforcadet")
    @Produces(MediaType.APPLICATION_JSON)
    public Response enqueueForCadet(@Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        String body = headerParams.getFirst("body");
        messagesAdministratorBean.enqueueForCadets(body);
        response = Response.ok("OK").build();
        return response;
    }
    
    @POST
    @Path("enqueueforclients")
    @Produces(MediaType.APPLICATION_JSON)
    public Response enqueueForClients(@Context HttpHeaders hh) {
        MultivaluedMap<String, String> headerParams = hh.getRequestHeaders();
        String emailsender = headerParams.getFirst("emailsender");
        String emailreceiver = headerParams.getFirst("emailreceiver");
        String body = headerParams.getFirst("body");
        messagesAdministratorBean.enqueueMailForClients(emailsender, emailreceiver, body);
        response = Response.ok("OK").build();
        return response;
    }
}
