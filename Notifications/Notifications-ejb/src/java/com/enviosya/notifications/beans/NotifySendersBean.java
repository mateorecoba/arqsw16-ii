package com.enviosya.notifications.beans;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MapMessage;
import com.enviosya.log.logger.CustomLogger;
import javax.ejb.EJB;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/Topic"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
    }) 
public class NotifySendersBean implements MessageListener {

    private CustomLogger log = new CustomLogger(NotifySendersBean.class);
    
    @EJB
    private EmailBean emailBean;
    
    public NotifySendersBean() {
    }

    @Override
    public void onMessage(Message message) {
        try {
            MapMessage msg = (MapMessage) message;
            String to = msg.getString("emailsender");
            String subject = msg.getString("subject");
            String body = msg.getString("body");
            System.out.println("Mensaje para Clientes Remitentes " + body);
            emailBean.sendEmail(to, subject, body);
            log.info("Recibiendo Mensaje para Clientes Remitentes " + body);
        } catch (JMSException ex) {
            log.error(ex);
        }
    }

}
