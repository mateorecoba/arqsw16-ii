
package com.enviosya.notifications.beans;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import com.enviosya.log.logger.CustomLogger;
import javax.ejb.LocalBean;
import javax.jms.MapMessage;

/**
 *
 * @author mateo
 */
@Singleton
@LocalBean
public class MessagesAdministratorBean implements MessagesAdministratorBeanLocal {
    
    private final CustomLogger log = new CustomLogger(MessagesAdministratorBean.class);

    @Resource(lookup = "jms/ConnectionFactory")
    private ConnectionFactory cf;

    @Resource(lookup = "jms/Queue")
    private Queue queue;

    @Resource(lookup = "jms/Topic")
    private Topic topic;   
    
    @Override
    public void enqueueForCadets(String text) {
        try {
            log.debug("Encolando mensaje " + text + " para Cadete");
            Connection connection = cf.createConnection();
            Session session = connection.createSession();
            TextMessage msg = session.createTextMessage(text);
            MessageProducer producer = session.createProducer(queue);
            producer.send(msg);
            session.close();
            connection.close();
            log.debug("Mensaje " + text + " para Cadete encolado correctamente");
        } catch (JMSException ex) {
            log.error(ex);
        }
    }
    
    @Override
    public void enqueueMailForClients(String emailSender, String emailReceiver, String text) {
        try {
            log.debug("Encolando mensaje " + text + " para Clientes");
            Connection connection = cf.createConnection();
            Session session = connection.createSession();
            MapMessage msg = session.createMapMessage();
            msg.setString("emailsender", emailSender);
            msg.setString("emailreceiver", emailReceiver);
            msg.setString("subject", "Detalle de envío");
            msg.setString("body", text);
            MessageProducer producer = session.createProducer(topic);
            producer.send(msg);
            session.close();
            connection.close();
            log.debug("Mensaje " + text + " para Cliente encolado correctamente");
        } catch (JMSException ex) {
            log.error(ex);
        }
    }

}