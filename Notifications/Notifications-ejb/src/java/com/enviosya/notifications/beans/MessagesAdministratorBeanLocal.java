package com.enviosya.notifications.beans;

import javax.ejb.Local;

/**
 *
 * @author Diego
 */
@Local
public interface MessagesAdministratorBeanLocal {
    
    void enqueueForCadets(String text);
    
    void enqueueMailForClients(String emailSender, String emailReceiver, String text);
    
}
