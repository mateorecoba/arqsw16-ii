package com.enviosya.notifications.beans;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import com.enviosya.log.logger.CustomLogger;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/Queue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
    })
public class NotifyCadetsBean implements MessageListener {

    private CustomLogger log = new CustomLogger(NotifyCadetsBean.class);    
    
    public NotifyCadetsBean() {
    }

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage msg = (TextMessage) message;
            System.out.println("Notificacion para Cadete = " + msg.getText());
            log.info("Recibiendo Mensaje para Cadete " + msg.getText());
        } catch (JMSException ex) {
            log.error(ex);
        }
    }

}
