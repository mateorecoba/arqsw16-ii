package com.enviosya.notifications.beans;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import com.enviosya.log.logger.CustomLogger;
import javax.ejb.EJB;
import javax.jms.MapMessage;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/Topic"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
    }) 
public class NotifyReceiversBean implements MessageListener {

    private CustomLogger log = new CustomLogger(NotifyReceiversBean.class);
    
    @EJB
    private EmailBean emailBean;
    
    public NotifyReceiversBean() {    
    }

    @Override
    public void onMessage(Message message) {
        try {
            MapMessage msg = (MapMessage) message;
            String to = msg.getString("emailreceiver");
            String subject = msg.getString("subject");
            String body = msg.getString("body");
            System.out.println("Mensaje para Clientes Receptores " + body);
            emailBean.sendEmail(to, subject, body);
            log.info("Recibiendo Mensaje para Clientes Receptores " + body);
        } catch (JMSException ex) {
            log.error(ex);
        }
    }

}
