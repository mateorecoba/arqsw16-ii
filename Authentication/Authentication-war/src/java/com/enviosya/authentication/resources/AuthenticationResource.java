package com.enviosya.authentication.resources;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.MediaType;
import com.enviosya.log.logger.CustomLogger;
import javax.ejb.EJB;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response;
import com.enviosya.authentication.beans.interfaces.AuthenticationBeanLocal;
import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author mateo
 */
@Path("authentication")
@RequestScoped
public class AuthenticationResource {

    private CustomLogger log = new CustomLogger(AuthenticationResource.class);

    @Context
    private UriInfo context;

    @EJB
    private AuthenticationBeanLocal authManagerBean;

    private final String errorStatus = "ERROR_STATUS";
    private final String errorMessage = "ERROR_MESSAGE";
    private final String token = "TOKEN";
    private final String userPasswordError = "USER_PASSWORD_ERROR";
    private final String userPasswordMessage = "The user or password are not valid";
    private final String internalError = "INTERNAL_ERROR";
    private final String internalMessage = "An error ocurred in server";
    private final String tokenNullError = "TOKEN_NULL";
    private final String tokenNullMessage = "Token cannot be null";
    private final String tokenError = "TOKEN_NOT_VALID";
    private final String tokenMessage = "Token is not valid";
    private final String tokenOkMessage = "USER_ID";

    /**
     * Creates a new instance of AuthenticationResource
     */
    public AuthenticationResource() {
        log.audit("Loading Authenticatin WS");
    }

    /**
     * Retrieves representation of an instance of
     * com.enviosya.authentication.resources.AuthenticationResource
     *
     * @return an instance of java.lang.String
     */
    @POST
    @Path(value = "logIn")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response logIn(@FormParam("user") String user,
            @FormParam("pass") String pass) {

        log.audit("Authenticating user " + user);
        Response response;

        String token = authManagerBean.generateToken(user, pass);

        if (token != null) {
            response = Response.status(Response.Status.OK)
                    .header(this.token, token)
                    .build();
            log.audit("Authentication success for user " + user);
        } else {
            response = Response.status(Response.Status.BAD_REQUEST)
                    .header(errorStatus, userPasswordError)
                    .header(errorMessage, userPasswordMessage)
                    .build();
            log.audit("Authentication not success for user " + user);
        }

        return response;
    }

    @POST
    @Path(value = "logOut")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response logOut(@FormParam("token") String token) {

        log.audit("Login out " + token);
        Response response = null;
        try {
            if (token != null) {
                if (authManagerBean.logOut(token)) {
                    response = Response.status(Response.Status.OK)
                            .build();
                    log.audit("Log out success for token " + token);
                } else {
                    response = Response.status(Response.Status.BAD_REQUEST)
                            .header(errorStatus, tokenError)
                            .header(errorMessage, tokenMessage)
                            .build();
                    log.audit("Log out not success for token " + token);
                }
            } else {
                response = Response.status(Response.Status.BAD_REQUEST)
                        .header(errorStatus, tokenNullError)
                        .header(errorMessage, tokenNullMessage)
                        .build();
                log.audit("Log out not success because of token null");
            }

        } catch (Exception e) {
            log.error(e);
            response = Response.status(Response.Status.BAD_REQUEST)
                    .header(errorStatus, internalError)
                    .header(errorMessage, internalMessage)
                    .build();
            log.audit("Log out not success for token " + token);
        }

        return response;
    }

    @GET
    @Path(value = "isAdminToken")
    public Response isAdminToken(@QueryParam("token") String token) {

        log.audit("Checking if is admin token " + token);
        Response response;

        Long userId = authManagerBean.isAdminToken(token);
        if (userId != null) {
            response = Response.status(Response.Status.OK)
                    .header(tokenOkMessage, userId)
                    .build();
            log.audit("Token is for valid admin " + token);
        } else {
            response = Response.status(Response.Status.BAD_REQUEST)
                    .header(errorMessage, "Token is not for admin user")
                    .build();
            log.audit("Token is not for valid admin " + token);
        }

        return response;
    }

    @GET
    @Path(value = "isUserToken")
    public Response isUserToken(@QueryParam("token") String token) {

        log.audit("Checking if is admin token " + token);
        Response response;
        Long userId = authManagerBean.isUserTokenToken(token);
        if (userId != null) {
            response = Response.status(Response.Status.OK)
                    .header(tokenOkMessage, userId)
                    .build();
            log.audit("Token is for valid user " + token);
        } else {
            response = Response.status(Response.Status.BAD_REQUEST)
                    .header(errorMessage, "Token is not for an user")
                    .build();
            log.audit("Token is not for valid user " + token);
        }

        return response;
    }

    @GET
    @Path(value = "getClientByToken")
    public Response getClientByToken(@QueryParam("token") String token) {
        log.audit("Checking if is a valid user " + token);
        log.audit("Call from " + context == null ? "null" : context.getAbsolutePath().toString());

        Response response;
        Long id = authManagerBean.getClientByToken(token);

        if (id != -1) {
            response = Response.ok(id).build();
            log.audit("Token is for valid user " + token);
        } else {
            response = Response.status(Response.Status.BAD_REQUEST)
                    .build();
            log.audit("Token is not for valid user " + token);
        }

        return response;
    }

}
