package com.enviosya.authentication.persistence;

import com.enviosya.authentication.entities.User;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mateo
 */
@Singleton
@LocalBean
public class AuthenticationEntityManagerBean {

//    @PersistenceContext
    private EntityManager entityManager;

    public User find(User user){
        return new User(1L, "usario.ejemplo@google.com", "123", true);
//        return entityManager.find(User.class, user);
    }
    
    public void persist(User user) {
        entityManager.persist(user);
    }

    public void merge(User aux) {
        entityManager.merge(aux);
    }

    public void remove(User user) {
        entityManager.remove(user);
    }


}