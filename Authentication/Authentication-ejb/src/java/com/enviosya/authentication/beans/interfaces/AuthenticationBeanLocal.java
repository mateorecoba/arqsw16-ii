package com.enviosya.authentication.beans.interfaces;

import javax.ejb.Local;

/**
 *
 * @author mateo
 */
@Local
public interface AuthenticationBeanLocal {
    
    
    Long getClientByToken(String token);

    String generateToken(String user, String pass);

    boolean logOut(String token);
    
    Long isAdminToken(String token);
    
    Long isUserTokenToken(String token);
    
}
