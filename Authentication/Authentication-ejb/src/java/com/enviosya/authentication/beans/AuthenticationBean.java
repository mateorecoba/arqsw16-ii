package com.enviosya.authentication.beans;

import com.enviosya.log.logger.CustomLogger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.util.Hashtable;
import java.util.UUID;
import com.enviosya.authentication.beans.interfaces.AuthenticationBeanLocal;
import com.enviosya.authentication.entities.User;
import com.enviosya.authentication.persistence.AuthenticationEntityManagerBean;
import com.enviosya.encryption.algorithm.Algorithm;
import com.enviosya.encryption.algorithm.AlgorithmBase64;
import java.io.IOException;
import java.security.GeneralSecurityException;
import javax.ejb.EJB;
import javax.ejb.Singleton;

/**
 *
 * @author mateo
 */
@Singleton
@LocalBean
public class AuthenticationBean implements AuthenticationBeanLocal {

    private CustomLogger log = new CustomLogger(AuthenticationBean.class);

    @EJB
    private AuthenticationEntityManagerBean authenticationEntityManagerBean;

    private Algorithm algorithm = new AlgorithmBase64();

    private Hashtable<Long, String> tokensByUsers = new Hashtable<Long, String>();

    private Hashtable<String, Long> usersByToken = new Hashtable<String, Long>();

    private Hashtable<String, Long> usersAdminByToken = new Hashtable<String, Long>();

    public AuthenticationBean() {
        log.audit("Authentication Manager bean initialized correctly");
    }

    @Override
    public Long getClientByToken(String token) {
        log.audit("Getting user ID by token " + token);

        Long id = getIdByToken(token);

        return id;
    }

    @Override
    public Long isUserTokenToken(String token) {
        log.audit("Checking if token exists " + token);
        
        Long userId = usersByToken.get(token);
        if (userId != null) {
            log.audit("User # " + userId + " found by token " + token);
        }
        
        return userId;
        
    }

    @Override
    public Long isAdminToken(String token) {
        log.audit("Checking if token exists for admin user " + token);
                
        Long userId = usersAdminByToken.get(token);
        if (userId != null) {
            log.audit("User Admin # " + userId + " found by token " + token);
        }
        
        return userId;
    }

    private Long getIdByToken(String token) {
        log.audit("Getting user ID by token");
        Long id = -1L;

        id = usersByToken.get(token);

        return id;
    }

    @Override
    public String generateToken(String userName, String pass) {
        log.audit("Generating token");
        String token = "";

        User user = checkUserPass(userName, pass);

        if (user != null) {
            token = UUID.randomUUID().toString();
            log.audit("Token generated " + token);

            try {
                tokensByUsers.put(user.getId(), token);
                usersByToken.put(token, user.getId());
                log.audit("Token inserted by User " + token);
                if (user.isAdmin()) {
                    usersAdminByToken.put(token, user.getId());
                    log.audit("Token inserted by Admin " + token);
                }
                
            } catch (Exception e) {
                log.error("Error trying to put token", e);
            }
        }

        return token;
    }

    private User checkUserPass(String userName, String pass) {
        log.audit("Checking Token for user " + userName);
        User user = new User();

        try {

            user.setUser(userName);
            String passEncrypted = algorithm.encrypt(pass);
            user.setPass(passEncrypted);
            user = authenticationEntityManagerBean.find(user);
        } catch (GeneralSecurityException ex) {
            log.error(ex);
        } catch (IOException ex) {
            log.error(ex);
        }

        return user;
    }

    @Override
    public boolean logOut(String token) {
        log.audit("Starting log out of token " + token);
        boolean isOk = false;
        Long userId = usersByToken.get(token);
        if (userId != null) {
            log.audit("User # " + userId + " found by token " + token);
            tokensByUsers.remove(userId);
            usersByToken.remove(token);
            usersAdminByToken.remove(token);
            isOk = true;
        }else{
            log.audit("User not found by token " + token);
        }

        return isOk;
    }

}
