package com.enviosya.authentication.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mateo
 */
//@Entity
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
//    @NotNull
    private String user;
    
//    @NotNull
    private String pass;
    
//    @NotNull
    private boolean admin;

    public User(Long id, String user, String pass, boolean admin) {
        this.id = id;
        this.user = user;
        this.pass = pass;
        this.admin = admin;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean isAdmin) {
        this.admin = isAdmin;
    }
    
    
    

    
}
